package kr.gracelove.springboot.domain.posts;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostsRepositoryTest {

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private EntityManager em;

    @Before
    public void setUp() {
        postsRepository.deleteAll();
    }

    @Test
    public void BaseTimeEntity_등록() throws Exception {
        //given
        LocalDateTime now = LocalDateTime.of(2019,6,4,0,0,0);
        postsRepository.save(Posts.builder()
                .title("title")
                .content("content")
                .author("author").build());

        //when
        List<Posts> postsList = postsRepository.findAll();

        //then
        Posts posts = postsList.get(0);
        System.out.println("createdDate : " + posts.getCreatedDate());
        System.out.println("modifiedDate : " + posts.getModifiedDate());
        Assertions.assertThat(posts.getCreatedDate()).isAfter(now);
        Assertions.assertThat(posts.getModifiedDate()).isAfter(now);
    }

    @Test
    public void 게시글저장_불러오기() throws Exception {
        //given
        String test_title = "test title";
        String test_content = "test content";
        String test_author = "gracelove91";

        Posts posts = Posts.builder()
                .title(test_title)
                .content(test_content)
                .author(test_author).build();

        //when
        Posts save = postsRepository.save(posts);

        em.flush();
        em.clear();

        Posts findPosts = postsRepository.findById(save.getId()).get();

        //then
        Assertions.assertThat(findPosts.getTitle()).isEqualTo(test_title);
        Assertions.assertThat(findPosts.getContent()).isEqualTo(test_content);
        Assertions.assertThat(findPosts.getAuthor()).isEqualTo(test_author);
     }
}