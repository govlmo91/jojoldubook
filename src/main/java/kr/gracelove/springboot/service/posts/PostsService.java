package kr.gracelove.springboot.service.posts;

import kr.gracelove.springboot.domain.posts.Posts;
import kr.gracelove.springboot.domain.posts.PostsRepository;
import kr.gracelove.springboot.web.dto.PostsUpdateRequestDto;
import kr.gracelove.springboot.web.dto.PostsResponseDto;
import kr.gracelove.springboot.web.dto.PostsSaveRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class PostsService {

    private final PostsRepository postsRepository;

    @Transactional
    public Long save(PostsSaveRequestDto requestDto) {
        return postsRepository.save(requestDto.toEntity()).getId();
    }

    @Transactional
    public Long update(Long id, PostsUpdateRequestDto requestDto) {

        Posts posts = postsRepository.findById(id).orElseThrow(() ->
            new IllegalArgumentException("해당 사용자가 없습니다. id : " + id)
        );

        posts.update(requestDto.getTitle(), requestDto.getContent());

        return id;
    }

    @Transactional
    public PostsResponseDto findById(Long id) {
        Posts posts = postsRepository.findById(id).orElseThrow(() ->
                new IllegalArgumentException("해당 사용자가 없습니다. id : " + id)
        );

        return new PostsResponseDto(posts);

    }
}
